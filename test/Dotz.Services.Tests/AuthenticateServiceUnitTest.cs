﻿using Dotz.Repositories.Interfaces;
using Dotz.Services.Tests.Fakers;
using Newtonsoft.Json;
using NSubstitute;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Dotz.Services.Tests
{
    public class AuthenticateServiceUnitTest
    {
        private static readonly string secret = Guid.NewGuid().ToString();
        private readonly IUserRepository _repository;

        public AuthenticateServiceUnitTest()
        {
            _repository = Substitute.For<IUserRepository>();
        }

        [Fact]
        public async Task Sucess_GetAsync_When_Parameters_Is_Correct()
        {
            //Arrange
            var result = AuthenticateFaker.GetFaker();
            var userFaker = UserFaker.GetFaker(result.Email, result.Password, result.Id);

            _repository.GetUserAsync(result.Email).Returns(userFaker);

            var service = new AuthenticateService(_repository);

            //Act
            var response = await service.GetAuthenticateAsync(result.Email, result.Password, secret);
            result.Token = response.Token;
            result.Password = string.Empty;

            //Assert
            Assert.Equal(JsonConvert.SerializeObject(result), JsonConvert.SerializeObject(response));
        }
    }
}
