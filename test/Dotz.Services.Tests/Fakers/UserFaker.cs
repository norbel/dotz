﻿using Bogus;
using Dotz.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Dotz.Services.Tests.Fakers
{
    public class UserFaker
    {
        public static User GetFaker() => GenerateFakeData().Generate();

        public static IEnumerable<User> GetFaker(int quantity) => GenerateFakeData().Generate(quantity);

        public static User GetFaker(string email, string password, Guid id) => GenerateFakeData(email, password, id).Generate();

        private static Faker<User> GenerateFakeData() =>
            new Faker<User>()
                .RuleFor(c => c.Id, f => Guid.NewGuid())
                .RuleFor(c => c.Email, f => f.Internet.Email())
                .RuleFor(c => c.Password, f => f.Internet.Password(8))
                .RuleFor(c => c.CreationDateTime, f => DateTime.Now);

        private static Faker<User> GenerateFakeData(string email, string password, Guid id) =>
            GenerateFakeData()
                .RuleFor(c => c.Id, f => id)
                .RuleFor(c => c.Email, f => email)
                .RuleFor(c => c.Password, f => BCrypt.Net.BCrypt.HashPassword(password));
    }
}
