﻿using Bogus;
using Dotz.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Dotz.Services.Tests.Fakers
{
    public class AuthenticateFaker
    {
        public static Authenticate GetFaker() => GenerateFakeData().Generate();

        public static IEnumerable<Authenticate> GetFaker(int quantity) => GenerateFakeData().Generate(quantity);

        private static Faker<Authenticate> GenerateFakeData() =>
            new Faker<Authenticate>()
                .RuleFor(c => c.Email, f => f.Internet.Email())
                .RuleFor(c => c.Id, f => Guid.NewGuid())
                .RuleFor(c => c.Password, f => f.Internet.Password(8))
                .RuleFor(c => c.Token, f => f.System.ApplePushToken());
    }
}
