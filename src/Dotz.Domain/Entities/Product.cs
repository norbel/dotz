﻿using Dotz.Domain.Keys;

namespace Dotz.Domain.Entities
{
    public class Product : Key
    {
        public string Description { get; set; }
        public decimal PointCost { get; set; }
        public bool Active { get; set; }
    }
}
