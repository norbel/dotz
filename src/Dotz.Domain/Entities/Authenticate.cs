﻿using Dotz.Domain.Keys;

namespace Dotz.Domain.Entities
{
    public class Authenticate : Key
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
    }
}
