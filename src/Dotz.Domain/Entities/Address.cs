﻿using Dotz.Domain.Keys;
using System;

namespace Dotz.Domain.Entities
{
    public class Address : Key
    {
        public string Street { get; set; }
        public string Number { get; set; }
        public string Neighborhodd { get; set; }
        public string Complement { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public Guid PersonId { get; set; }

        public virtual Person Person { get; set; }
        public virtual RemovePoints RemovePoints { get; set; }
    }
}
