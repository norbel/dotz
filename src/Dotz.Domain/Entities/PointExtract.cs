﻿using Dotz.Domain.Keys;
using System;

namespace Dotz.Domain.Entities
{
    public class PointExtract : Key
    {
        public Guid PersonId { get; set; }
        public decimal Points { get; set; }
        public DateTime CreationDate { get; set; }

        public virtual Person Person{ get; set; }
        public virtual InsertPoints InsertPoints { get; set; }
        public virtual RemovePoints RemovePoints { get; set; }
    }
}
