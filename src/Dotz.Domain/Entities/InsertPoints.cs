﻿using Dotz.Domain.Keys;
using System;

namespace Dotz.Domain.Entities
{
    public class InsertPoints : Key
    {
        public Guid PartnerId { get; set; }
        public Guid PointExtractId { get; set; }
        public bool Aproved { get; set; }

        public virtual Partner Partner { get; set; }
        public virtual PointExtract PointExtract { get; set; }
    }
}
