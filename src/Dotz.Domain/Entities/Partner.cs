﻿using Dotz.Domain.Keys;

namespace Dotz.Domain.Entities
{
    public class Partner : Key
    {
        public string CompanyName { get; set; }

        public virtual InsertPoints InsertPoints { get; set; }
    }
}
