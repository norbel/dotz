﻿using Dotz.Domain.Keys;
using System;

namespace Dotz.Domain.Entities
{
    public class User : Key
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public Guid PersonId { get; set; }
        public DateTime CreationDateTime { get; set; }

        public virtual Person Person { get; set; }
    }
}
