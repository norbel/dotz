﻿using Dotz.Domain.Keys;
using System;
using System.Collections.Generic;

namespace Dotz.Domain.Entities
{
    public class Person : Key
    {
        public string Name { get; set; }
        public DateTime? Birth { get; set; }
        public string Cpf { get; set; }
        public decimal PointsBalance { get; set; }

        public virtual ICollection<PointExtract> Extract { get; set; }
        public virtual ICollection<Address> Addresses { get; set; }
        public virtual User User { get; set; }
    }
}
