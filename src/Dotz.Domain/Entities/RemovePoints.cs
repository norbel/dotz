﻿using Dotz.Domain.Keys;
using System;

namespace Dotz.Domain.Entities
{
    public class RemovePoints : Key
    {
        public Guid AddressId { get; set; }
        public Guid PointExtractId { get; set; }
        public bool Cancelled { get; set; }

        public virtual Address Address { get; set; }
        public virtual PointExtract PointExtract { get; set; }
    }
}
