﻿using System;

namespace Dotz.Domain.Keys
{
    public abstract class Key
    {
        public Guid Id { get; set; }
    }
}
