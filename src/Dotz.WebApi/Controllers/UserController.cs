﻿using Dotz.Services.Interfaces;
using Dotz.Services.ViewModels;
using Dotz.Services.ViewModels.User;
using Dotz.WebApi.Properties;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dotz.WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IPersonService _personService;

        public UserController(IUserService userService, IPersonService personService)
        {
            _userService = userService;
            _personService = personService;
        }

        [HttpGet]
        [Route("{id}/extract")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<ExtractViewModel>>> GetExtractAsync(Guid id)
        {
            try
            {
                var result = await _personService.GetExtractAsync(id);

                if (result == null)
                    return NotFound(new MessageResponse { Message = string.Format(Resources.NotFound, nameof(User)) });

                return Ok(result);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    new MessageResponse { Message = Resources.ServerError });
            }
        }

        [HttpGet]
        [Route("{id}/balance")]
        [Authorize]
        public async Task<ActionResult<BalanceViewModel>> GetBalanceAsync(Guid id)
        {
            try
            {
                var result = await _personService.GetBalanceAsync(id);

                if (result == null)
                    return NotFound(new MessageResponse { Message = string.Format(Resources.NotFound, nameof(User)) });

                return Ok(new BalanceViewModel { Balance = result.Value });
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    new MessageResponse { Message = Resources.ServerError });
            }
        }

        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<UserRequest>> Get(Guid id)
        {
            try
            {
                var result = await _userService.GetUserByIdAsync(id);

                if (result == null)
                    return NotFound(new MessageResponse { Message = string.Format(Resources.NotFound, nameof(User)) });

                return Ok(result);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    new MessageResponse { Message = Resources.ServerError });
            }
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<UserRequest>> Post([FromBody] UserRequest user)
        {
            try
            {
                var result = await _userService.CreateUserAsync(user);

                if (result == null)
                    StatusCode(StatusCodes.Status500InternalServerError,
                    new MessageResponse { Message = string.Format(Resources.NotCreated, nameof(User)) });

                return Ok(result);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    new MessageResponse { Message = Resources.ServerError });
            }
        }

        [HttpPut("{id}")]
        [Authorize]
        public async Task<ActionResult<MessageResponse>> Put(Guid id, [FromBody] UserRequest user)
        {
            try
            {
                if (await _userService.UpdateAsync(id, user))
                    return Ok(new MessageResponse { Message = string.Format(Resources.SucessUpdate, nameof(User)) });
                else
                    return NotFound(new MessageResponse { Message = string.Format(Resources.ErrorUpdate, nameof(User)) });
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    new MessageResponse { Message = Resources.ServerError });
            }
        }

        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult<MessageResponse>> Delete(Guid id)
        {
            try
            {
                if (await _userService.DeleteUserByIdAsync(id))
                    return Ok(new MessageResponse { Message = string.Format(Resources.SucessDelete, nameof(User)) });
                else
                    return NotFound(new MessageResponse { Message = string.Format(Resources.ErrorDelete, nameof(User)) });
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    new MessageResponse { Message = Resources.ServerError });
            }
        }
    }
}
