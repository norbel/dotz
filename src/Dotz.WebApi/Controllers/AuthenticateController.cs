﻿using Dotz.Domain.Entities;
using Dotz.Services.Interfaces;
using Dotz.Services.ViewModels;
using Dotz.WebApi.Properties;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace Dotz.WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AuthenticateController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IAuthenticateService _service;

        public AuthenticateController(IConfiguration configuration, IAuthenticateService service)
        {
            _configuration = configuration;
            _service = service;
        }

        [HttpPost]
        [Route("login")]
        public async Task<ActionResult<Authenticate>> Authenticate([FromBody] AuthenticateLogin model)
        {
            try
            {
                var result = await _service.GetAuthenticateAsync(model.Email, model.Password, _configuration.GetSection("Secret").Value);

                if (result == null)
                    return NotFound(new { message = Resources.IncorrectAuthentication });

                return Ok(result);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { message = "Internal Server Error" });
            }
        }

        [HttpGet]
        [Route("authenticated")]
        [Authorize]
        public ActionResult<string> Authenticated() =>
            Ok(new { message = string.Format("Autenticado - {0}", User.Identity.Name) });
    }
}
