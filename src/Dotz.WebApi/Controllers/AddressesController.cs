﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Dotz.Domain.Entities;
using Dotz.Repositories;
using Dotz.Services.Interfaces;
using Dotz.Services.ViewModels;
using Dotz.WebApi.Properties;
using Dotz.Services.ViewModels.Address;

namespace Dotz.WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AddressesController : ControllerBase
    {
        private readonly IAddressService _addressService;

        public AddressesController(IAddressService addressService)
        {
            _addressService = addressService;
        }

        [HttpPost]
        public async Task<ActionResult<Address>> PostAddress(AddressRequest address)
        {
            try
            {
                var result = await _addressService.CreateAddressAsync(address);

                if (result == null)
                    StatusCode(StatusCodes.Status500InternalServerError,
                    new MessageResponse { Message = string.Format(Resources.NotCreated, nameof(Address)) });

                return Ok(result);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    new MessageResponse { Message = Resources.ServerError });
            }
        }


        [HttpDelete("{id}")]
        public async Task<ActionResult<Address>> DeleteAddress(Guid id)
        {
            try
            {
                if (await _addressService.DeleteAddressByIdAsync(id))
                    return Ok(new MessageResponse { Message = string.Format(Resources.SucessDelete, nameof(Address)) });
                else
                    return NotFound(new MessageResponse { Message = string.Format(Resources.ErrorDelete, nameof(Address)) });
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    new MessageResponse { Message = Resources.ServerError });
            }
        }
    }
}
