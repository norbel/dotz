﻿using AutoMapper;
using Dotz.Domain.Entities;
using Dotz.Repositories.Interfaces;
using Dotz.Services.Interfaces;
using Dotz.Services.ViewModels.Address;
using System;
using System.Threading.Tasks;

namespace Dotz.Services
{
    public class AddressService : IAddressService
    {
        private readonly IAddressRepository _repository;
        private readonly IMapper _mapper;

        public AddressService(IAddressRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<AddressViewModel> CreateAddressAsync(AddressRequest addressRequest)
        {
            var address = _mapper.Map<AddressRequest, Address>(addressRequest);

            await _repository.AddAsync(address);

            if (await _repository.SaveAsync())
                return _mapper.Map<Address, AddressViewModel>(address);
            else
                return null;
        }

        public async Task<bool> DeleteAddressByIdAsync(Guid id)
        {
            await _repository.RemoveByIdAsync(id);
            return await _repository.SaveAsync();
        }
    }
}
