﻿using AutoMapper;
using Dotz.Domain.Entities;
using Dotz.Repositories.Interfaces;
using Dotz.Services.Interfaces;
using Dotz.Services.ViewModels.User;
using System;
using System.Threading.Tasks;

namespace Dotz.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<UserViewModel> GetUserByIdAsync(Guid id)
        {
            var user = await _userRepository.GetUserByIdAsync(id);
            return _mapper.Map<User, UserViewModel>(user);
        }

        public async Task<UserViewModel> CreateUserAsync(UserRequest userCreate)
        {
            var user = _mapper.Map<UserRequest, User>(userCreate);

            user.Password = BCrypt.Net.BCrypt.HashPassword(user.Password);
            await _userRepository.AddAsync(user);

            if (await _userRepository.SaveAsync())
                return _mapper.Map<User, UserViewModel>(user);
            else
                return null;
        }

        public async Task<bool> DeleteUserByIdAsync(Guid id)
        {
            await _userRepository.RemoveByIdAsync(id);
            return await _userRepository.SaveAsync();
        }

        public async Task<bool> UpdateAsync(Guid id, UserRequest userUpdate)
        {
            var user = _mapper.Map<UserRequest, User>(userUpdate);

            user.PersonId = id;
            await _userRepository.UpdateUserAsync(user);

            return await _userRepository.SaveAsync();
        }
    }
}
