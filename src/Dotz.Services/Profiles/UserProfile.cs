﻿using AutoMapper;
using Dotz.Domain.Entities;
using Dotz.Services.ViewModels.User;

namespace Dotz.Services.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserRequest, User>()
                .ForMember(d => d.Person.Birth, o => o.MapFrom(r => r.Birth))
                .ForMember(d => d.Person.Cpf, o => o.MapFrom(r => r.Cpf))
                .ForMember(d => d.Email, o => o.MapFrom(r => r.Email))
                .ForMember(d => d.Password, o => o.MapFrom(r => r.Password))
                .ForMember(d => d.Person.Name, o => o.MapFrom(r => r.Name))
                .ForMember(d => d.Person.PointsBalance, o => o.MapFrom(r => r.PointsBalance))
                .ReverseMap();

            CreateMap<User, UserViewModel>()
                .ForMember(d => d.Birth, o => o.MapFrom(r => r.Person.Birth))
                .ForMember(d => d.Cpf, o => o.MapFrom(r => r.Person.Cpf))
                .ForMember(d => d.Email, o => o.MapFrom(r => r.Email))
                .ForMember(d => d.Password, o => o.MapFrom(r => r.Password))
                .ForMember(d => d.Name, o => o.MapFrom(r => r.Person.Name))
                .ForMember(d => d.Id, o => o.MapFrom(r => r.Person.Id))
                .ForMember(d => d.PointsBalance, o => o.MapFrom(r => r.Person.PointsBalance))
                .ForMember(d => d.Addresses, o => o.MapFrom(r => r.Person.Addresses))
                .ForMember(d => d.Extract, o => o.MapFrom(r => r.Person.Extract))
                .ReverseMap();

            CreateMap<InsertPoints, InsertPointsViewModel>()
                .ForMember(d => d.Aproved, o => o.MapFrom(r => r.Aproved))
                .ForMember(d => d.CompanyName, o => o.MapFrom(r => r.Partner.CompanyName))
                .ReverseMap();

            CreateMap<RemovePoints, RemovePointsViewModel>()
                .ForMember(d => d.AddressDelivery, o => o.MapFrom(r => r.Address))
                .ForMember(d => d.CancelledDelivery, o => o.MapFrom(r => r.Cancelled))
                .ReverseMap();

            CreateMap<PointExtract, ExtractViewModel>().ReverseMap();    
        }
    }
}
