﻿using AutoMapper;
using Dotz.Domain.Entities;
using Dotz.Services.ViewModels.Address;

namespace Dotz.Services.Profiles
{
    public class AddressProfile : Profile
    {
        public AddressProfile()
        {
            CreateMap<AddressRequest, Address>()
                .ForMember(c => c.PersonId, o => o.MapFrom(r => r.UserId))
                .ReverseMap();

            CreateMap<Address, AddressViewModel>()
                .ForMember(c => c.UserId, o => o.MapFrom(r => r.PersonId))
                .ReverseMap();
        }
    }
}
