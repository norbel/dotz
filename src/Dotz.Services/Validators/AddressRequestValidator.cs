﻿using Dotz.Services.ViewModels.Address;
using FluentValidation;

namespace Dotz.Services.Validators
{
    public class AddressRequestValidator : AbstractValidator<AddressRequest>
    {
        private const string ErrorMessage = "Please enter the {0}.";

        public AddressRequestValidator()
        {
            RuleFor(c => c.City)
                .NotEmpty().WithMessage(string.Format(ErrorMessage, nameof(AddressRequest.City)))
                .NotNull().WithMessage(string.Format(ErrorMessage, nameof(AddressRequest.City)));

            RuleFor(c => c.Street)
                .NotEmpty().WithMessage(string.Format(ErrorMessage, nameof(AddressRequest.Street)))
                .NotNull().WithMessage(string.Format(ErrorMessage, nameof(AddressRequest.Street)));

            RuleFor(c => c.UserId)
                .NotEmpty().WithMessage(string.Format(ErrorMessage, nameof(AddressRequest.UserId)))
                .NotNull().WithMessage(string.Format(ErrorMessage, nameof(AddressRequest.UserId)));
        }
    }
}
