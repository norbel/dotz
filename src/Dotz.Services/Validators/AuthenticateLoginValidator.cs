﻿using Dotz.Services.ViewModels;
using FluentValidation;

namespace Dotz.Services.Validators
{
    public class AuthenticateLoginValidator : AbstractValidator<AuthenticateLogin>
    {
        private const string ErrorMessage = "Please enter the {0}.";

        public AuthenticateLoginValidator()
        {
            RuleFor(c => c.Email)
                .NotEmpty().WithMessage(string.Format(ErrorMessage, nameof(AuthenticateLogin.Email)))
                .NotNull().WithMessage(string.Format(ErrorMessage, nameof(AuthenticateLogin.Email)));

            RuleFor(c => c.Password)
                .NotEmpty().WithMessage(string.Format(ErrorMessage, nameof(AuthenticateLogin.Password)))
                .NotNull().WithMessage(string.Format(ErrorMessage, nameof(AuthenticateLogin.Password)));
        }
    }
}
