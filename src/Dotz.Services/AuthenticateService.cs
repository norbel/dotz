﻿using Dotz.Domain.Entities;
using Dotz.Repositories.Interfaces;
using Dotz.Services.Interfaces;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Dotz.Services
{
    public class AuthenticateService : IAuthenticateService
    {
        private readonly IUserRepository _repository;

        public AuthenticateService(IUserRepository repository)
        {
            _repository = repository;
        }

        public async Task<Authenticate> GetAuthenticateAsync(string email, string password, string secret)
        {
            var user = await _repository.GetUserAsync(email);

            if (user != null && BCrypt.Net.BCrypt.Verify(password, user.Password)) 
                return SetAuthenticate(secret, user);
            else
                return null;
        }

        public async Task<Authenticate> GetByIdAsync(Guid id, string secret)
        {
            var user = await _repository.GetByIdAsync(id);

            if (user != null)
                return SetAuthenticate(secret, user);
            else
                return null;
        }

        private Authenticate SetAuthenticate(string secret, User user)
        {
            return new Authenticate
            {
                Email = user.Email,
                Id = user.Id,
                Password = string.Empty,
                Token = GenerateToken(user, secret)
            };
        }

        private string GenerateToken(User user, string secret)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Email, user.Email)
                }),
                Expires = DateTime.UtcNow.AddHours(6),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
