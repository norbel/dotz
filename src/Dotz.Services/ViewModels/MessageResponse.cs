﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dotz.Services.ViewModels
{
    public class MessageResponse
    {
        public string Message { get; set; }
    }
}
