﻿namespace Dotz.Services.ViewModels
{
    public class AuthenticateLogin
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
