﻿namespace Dotz.Services.ViewModels.User
{
    public class BalanceViewModel
    {
        public decimal Balance { get; set; }
    }
}
