﻿using Dotz.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Dotz.Services.ViewModels.User
{
    public class UserViewModel
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
        public DateTime? Birth { get; set; }
        public string Cpf { get; set; }
        public decimal PointsBalance { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public ICollection<PointExtract> Extract { get; set; }
        public ICollection<Domain.Entities.Address> Addresses { get; set; }
    }
}
