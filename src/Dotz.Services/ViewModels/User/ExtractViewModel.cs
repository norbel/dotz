﻿using System;

namespace Dotz.Services.ViewModels.User
{
    public class ExtractViewModel
    {
        public decimal Points { get; set; }
        public DateTime CreationDate { get; set; }
        public InsertPointsViewModel InsertPoints { get; set; }
        public RemovePointsViewModel RemovePoints { get; set; }
    }

    public class InsertPointsViewModel
    {
        public string CompanyName { get; set; }
        public bool Aproved { get; set; }
    }

    public class RemovePointsViewModel
    {
        public bool CancelledDelivery { get; set; }
        public Address.AddressViewModel AddressDelivery { get; set; }
    }
}
