﻿using System;

namespace Dotz.Services.ViewModels.User
{
    public class UserRequest
    {
        public string Name { get; set; }
        public DateTime? Birth { get; set; }
        public string Cpf { get; set; }
        public decimal PointsBalance { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
