﻿using System;

namespace Dotz.Services.ViewModels.Address
{
    public class AddressViewModel
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string Street { get; set; }
        public string Number { get; set; }
        public string Neighborhodd { get; set; }
        public string Complement { get; set; }
        public string City { get; set; }
        public string State { get; set; }
    }
}
