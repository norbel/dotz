﻿using Dotz.Domain.Entities;
using System;
using System.Threading.Tasks;

namespace Dotz.Services.Interfaces
{
    public interface IAuthenticateService
    {
        Task<Authenticate> GetAuthenticateAsync(string email, string password, string secret);
        Task<Authenticate> GetByIdAsync(Guid id, string secret);
    }
}
