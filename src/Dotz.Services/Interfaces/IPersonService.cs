﻿using Dotz.Services.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dotz.Services.Interfaces
{
    public interface IPersonService
    {
        Task<decimal?> GetBalanceAsync(Guid id);
        Task<IEnumerable<ExtractViewModel>> GetExtractAsync(Guid id);
    }
}