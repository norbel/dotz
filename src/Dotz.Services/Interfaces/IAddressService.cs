﻿using Dotz.Services.ViewModels.Address;
using System;
using System.Threading.Tasks;

namespace Dotz.Services.Interfaces
{
    public interface IAddressService
    {
        Task<AddressViewModel> CreateAddressAsync(AddressRequest addressRequest);
        Task<bool> DeleteAddressByIdAsync(Guid id);
    }
}
