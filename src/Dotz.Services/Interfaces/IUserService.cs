﻿using Dotz.Services.ViewModels.User;
using System;
using System.Threading.Tasks;

namespace Dotz.Services.Interfaces
{
    public interface IUserService
    {
        Task<UserViewModel> GetUserByIdAsync(Guid id);
        Task<UserViewModel> CreateUserAsync(UserRequest userCreate);
        Task<bool> DeleteUserByIdAsync(Guid id);
        Task<bool> UpdateAsync(Guid id, UserRequest user);
    }
}
