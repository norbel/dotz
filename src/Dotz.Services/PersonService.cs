﻿using AutoMapper;
using Dotz.Domain.Entities;
using Dotz.Repositories.Interfaces;
using Dotz.Services.Interfaces;
using Dotz.Services.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dotz.Services
{
    public class PersonService : IPersonService
    {
        private readonly IPersonRepository _personRepository;
        private readonly IMapper _mapper;

        public PersonService(IPersonRepository personRepository, IMapper mapper)
        {
            _personRepository = personRepository;
            _mapper = mapper;
        }

        public async Task<decimal?> GetBalanceAsync(Guid id)
        {
            try
            {
                var user = await _personRepository.GetByIdAsync(id);
                return user?.PointsBalance;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<IEnumerable<ExtractViewModel>> GetExtractAsync(Guid id)
        {
            List<ExtractViewModel> result = null;
            var person = await _personRepository.GetExtractAsync(id);
            foreach (var item in person.Extract)
            {
                result.Add(_mapper.Map<PointExtract, ExtractViewModel>(item));
            }
            return result.ToArray();
        }
    }
}
