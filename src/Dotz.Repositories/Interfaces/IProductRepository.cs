﻿using Dotz.Domain.Entities;

namespace Dotz.Repositories.Interfaces
{
    public interface IProductRepository : IGenericRepository<Product>
    {
    }
}
