﻿using Dotz.Domain.Entities;

namespace Dotz.Repositories.Interfaces
{
    public interface IRemovePointsRepository : IGenericRepository<RemovePoints>
    {
    }
}
