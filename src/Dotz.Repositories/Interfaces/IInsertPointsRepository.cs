﻿using Dotz.Domain.Entities;

namespace Dotz.Repositories.Interfaces
{
    public interface IInsertPointsRepository : IGenericRepository<InsertPoints>
    {
    }
}
