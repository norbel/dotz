﻿using Dotz.Domain.Entities;
using System;
using System.Threading.Tasks;

namespace Dotz.Repositories.Interfaces
{
    public interface IUserRepository : IGenericRepository<User>
    {
        Task<User> GetUserByIdAsync(Guid id);
        Task<User> GetUserAsync(string email);
        Task UpdateUserAsync(User user);
    }
}
