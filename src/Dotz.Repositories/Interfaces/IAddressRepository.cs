﻿using Dotz.Domain.Entities;

namespace Dotz.Repositories.Interfaces
{
    public interface IAddressRepository : IGenericRepository<Address>
    {
    }
}
