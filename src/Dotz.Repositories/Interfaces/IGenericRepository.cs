﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Dotz.Repositories.Interfaces
{
    public interface IGenericRepository<T> where T : class
    {
        Task<T> GetByIdAsync(Guid id);
        Task<int> CountAsync(Expression<Func<T, bool>> filter);

        Task AddAsync(T entity);
        void Update(T entity);
        void Remove(T entity);
        Task RemoveByIdAsync(Guid id);

        Task<bool> SaveAsync();
    }
}
