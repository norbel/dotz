﻿using Dotz.Domain.Entities;

namespace Dotz.Repositories.Interfaces
{
    public interface IPartnerRepository : IGenericRepository<Partner>
    {
    }
}
