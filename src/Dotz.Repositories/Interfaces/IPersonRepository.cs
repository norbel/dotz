﻿using Dotz.Domain.Entities;
using System;
using System.Threading.Tasks;

namespace Dotz.Repositories.Interfaces
{
    public interface IPersonRepository : IGenericRepository<Person>
    {
        Task<Person> GetExtractAsync(Guid id);
    }
}
