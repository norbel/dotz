﻿using Dotz.Domain.Entities;

namespace Dotz.Repositories.Interfaces
{
    public interface IPointExtractRepository : IGenericRepository<PointExtract>
    {
    }
}
