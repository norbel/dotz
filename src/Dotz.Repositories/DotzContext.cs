﻿using Dotz.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Dotz.Repositories
{
    public class DotzContext : DbContext
    {
        public DotzContext(DbContextOptions<DotzContext> options) : base(options)
        {
        }

        public DbSet<Address> Address { get; set; }
        public DbSet<InsertPoints> InsertPoints { get; set; }
        public DbSet<Partner> Partners { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<PointExtract> PointExtract { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<RemovePoints> RemovePoints { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(DotzContext).Assembly);
        }
    }
}
