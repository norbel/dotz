﻿using Dotz.Domain.Entities;
using Dotz.Repositories.Interfaces;

namespace Dotz.Repositories.Repository
{
    public class PartnerRepository : GenericRepository<Partner>, IPartnerRepository
    {
        public PartnerRepository(DotzContext dbContext) : base(dbContext)
        {
        }
    }
}
