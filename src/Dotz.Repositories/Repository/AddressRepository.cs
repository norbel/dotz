﻿using Dotz.Domain.Entities;
using Dotz.Repositories.Interfaces;

namespace Dotz.Repositories.Repository
{
    public class AddressRepository : GenericRepository<Address>, IAddressRepository
    {
        public AddressRepository(DotzContext dbContext) : base(dbContext)
        {
        }
    }
}
