﻿using Dotz.Domain.Entities;
using Dotz.Repositories.Interfaces;

namespace Dotz.Repositories.Repository
{
    public class RemovePointsRepository : GenericRepository<RemovePoints>, IRemovePointsRepository
    {
        public RemovePointsRepository(DotzContext dbContext) : base(dbContext)
        {
        }
    }
}
