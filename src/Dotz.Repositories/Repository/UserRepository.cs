﻿using Dotz.Domain.Entities;
using Dotz.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Dotz.Repositories.Repository
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(DotzContext dbContext) : base(dbContext)
        {
        }

        public Task<User> GetUserByIdAsync(Guid id)
        {
            return _dbSet
                .Include(u => u.Person)
                .Include(u => u.Person.Addresses)
                .SingleOrDefaultAsync(p => p.Person.Id == id);
        }

        public async Task<User> GetUserAsync(string email)
        {
            return await _dbSet.SingleOrDefaultAsync(u => u.Email == email);
        }

        public async Task UpdateUserAsync(User user)
        {
            var userConsulted = await GetByIdAsync(user.Id);

            _context.Entry(userConsulted).CurrentValues.SetValues(user);
        }
    }
}
