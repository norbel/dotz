﻿using Dotz.Domain.Entities;
using Dotz.Repositories.Interfaces;

namespace Dotz.Repositories.Repository
{
    public class InsertPointsRepository : GenericRepository<InsertPoints>, IInsertPointsRepository
    {
        public InsertPointsRepository(DotzContext dbContext) : base(dbContext)
        {
        }
    }
}
