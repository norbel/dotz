﻿using Dotz.Domain.Entities;
using Dotz.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Dotz.Repositories.Repository
{
    public class PersonRepository : GenericRepository<Person>, IPersonRepository
    {
        public PersonRepository(DotzContext dbContext) : base(dbContext)
        {
        }

        public async Task<Person> GetExtractAsync(Guid id)
        {
            var person = await GetByIdAsync(id);

            return await _dbSet.Include(p => p.Extract).SingleOrDefaultAsync(p => p.Id == id);
        }
    }
}
