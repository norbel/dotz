﻿using Dotz.Domain.Entities;
using Dotz.Repositories.Interfaces;

namespace Dotz.Repositories.Repository
{
    public class PointExtractRepository : GenericRepository<PointExtract>, IPointExtractRepository
    {
        public PointExtractRepository(DotzContext dbContext) : base(dbContext)
        {
        }
    }
}
