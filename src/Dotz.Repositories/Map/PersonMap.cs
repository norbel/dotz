﻿using Dotz.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dotz.Repositories.Map
{
    public class PersonMap : IEntityTypeConfiguration<Person>
    {
        private const string TableName = "Person";

        public void Configure(EntityTypeBuilder<Person> builder)
        {
            builder.ToTable(TableName);
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Birth);
            builder.Property(e => e.Cpf).IsRequired();
            builder.Property(e => e.Name).IsRequired();
            builder.Property(e => e.PointsBalance).HasDefaultValue(0).IsRequired();

            builder.HasMany(e => e.Extract).WithOne(f => f.Person)
                .HasForeignKey(f => f.PersonId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasMany(e => e.Addresses).WithOne(f => f.Person)
                .HasForeignKey(f => f.PersonId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasOne(e => e.User).WithOne(f => f.Person)
                .HasForeignKey<User>(d => d.PersonId)
                .OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}
