﻿using Dotz.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace Dotz.Repositories.Map
{
    public class PointExtractMap : IEntityTypeConfiguration<PointExtract>
    {
        private const string TableName = "PointExtract";

        public void Configure(EntityTypeBuilder<PointExtract> builder)
        {
            builder.ToTable(TableName);
            builder.HasKey(e => e.Id);
            builder.Property(e => e.CreationDate).HasDefaultValue(DateTime.Now).ValueGeneratedOnAdd().IsRequired();
            builder.Property(e => e.PersonId).IsRequired();
            builder.Property(e => e.Points).IsRequired();

            builder.HasOne(e => e.InsertPoints).WithOne(f => f.PointExtract)
                .HasForeignKey<InsertPoints>(f => f.PointExtractId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasOne(e => e.RemovePoints).WithOne(f => f.PointExtract)
                .HasForeignKey<RemovePoints>(d => d.PointExtractId)
                .OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}
