﻿using Dotz.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dotz.Repositories.Map
{
    public class ProductMap : IEntityTypeConfiguration<Product>
    {
        private const string TableName = "Product";

        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable(TableName);
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Active).IsRequired();
            builder.Property(e => e.Description).IsRequired();
            builder.Property(e => e.PointCost).IsRequired();
        }
    }
}
