﻿using Dotz.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dotz.Repositories.Map
{
    public class PartnerMap : IEntityTypeConfiguration<Partner>
    {
        private const string TableName = "Partner";

        public void Configure(EntityTypeBuilder<Partner> builder)
        {
            builder.ToTable(TableName);
            builder.HasKey(e => e.Id);
            builder.Property(e => e.CompanyName).IsRequired();

            builder.HasOne(e => e.InsertPoints).WithOne(f => f.Partner)
                .HasForeignKey<InsertPoints>(f => f.PartnerId)
                .OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}
