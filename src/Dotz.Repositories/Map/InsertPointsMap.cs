﻿using Dotz.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dotz.Repositories.Map
{
    public class InsertPointsMap : IEntityTypeConfiguration<InsertPoints>
    {
        private const string TableName = "InsertPoints";

        public void Configure(EntityTypeBuilder<InsertPoints> builder)
        {
            builder.ToTable(TableName);
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Aproved).IsRequired();
            builder.Property(e => e.PartnerId).IsRequired();
            builder.Property(e => e.PointExtractId).IsRequired();
        }
    }
}
