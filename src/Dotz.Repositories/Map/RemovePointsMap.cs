﻿using Dotz.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dotz.Repositories.Map
{
    public class RemovePointsMap : IEntityTypeConfiguration<RemovePoints>
    {
        private const string TableName = "RemovePoints";

        public void Configure(EntityTypeBuilder<RemovePoints> builder)
        {
            builder.ToTable(TableName);
            builder.HasKey(e => e.Id);
            builder.Property(e => e.AddressId).IsRequired();
            builder.Property(e => e.Cancelled).IsRequired();
            builder.Property(e => e.PointExtractId).IsRequired();
        }
    }
}
