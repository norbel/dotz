﻿using Dotz.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace Dotz.Repositories.Map
{
    public class UserMap : IEntityTypeConfiguration<User>
    {
        private const string TableName = nameof(User);

        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable(TableName);
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Email).IsRequired();
            builder.HasIndex(e => e.Email).IsUnique();
            builder.Property(e => e.Password).IsRequired();
            builder.Property(e => e.CreationDateTime).HasDefaultValue(DateTime.Now).ValueGeneratedOnAdd();
        }
    }
}
