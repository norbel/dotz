﻿using Dotz.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dotz.Repositories.Map
{
    public class AddressMap : IEntityTypeConfiguration<Address>
    {
        private const string TableName = "Address";

        public void Configure(EntityTypeBuilder<Address> builder)
        {
            builder.ToTable(TableName);
            builder.HasKey(e => e.Id);
            builder.Property(e => e.City).IsRequired();
            builder.Property(e => e.Complement);
            builder.Property(e => e.Neighborhodd);
            builder.Property(e => e.PersonId).IsRequired();

            builder.HasOne(e => e.RemovePoints).WithOne(f => f.Address)
                .HasForeignKey<RemovePoints>(d => d.AddressId)
                .OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}
