﻿using Dotz.Domain.Entities;
using System;

namespace Dotz.Repositories
{
    public static class InitialDataDb
    {
        public static Person InitialDataPerson => new Person
        {
            Id = Guid.NewGuid(),
            Birth = DateTime.Parse("1986-09-06"),
            Cpf = "01234567890",
            Name = "Lucas Vicente Leite",
            PointsBalance = 0,
            User = new User
            {
                Id = Guid.NewGuid(),
                Email = @"admin@dotz.com.br",
                Password = "ContratarLucasEm06-2021"
            }
        };
    }
}
